package com.itea;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ResourceBundle;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pudge {
    private int level;
    private String name;
    private String[] phrases;
    private transient ResourceBundle resource;

    public Pudge(Pudge pudge) {
        this.level = pudge.level;
        this.name = pudge.name;
        if (pudge.phrases != null) {
            this.phrases = new String[pudge.phrases.length];
            System.arraycopy(pudge.phrases, 0, this.phrases, 0, pudge.phrases.length);
        }
        this.resource = pudge.resource;
    }
}

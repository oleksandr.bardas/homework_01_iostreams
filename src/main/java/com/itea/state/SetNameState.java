package com.itea.state;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Scanner;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SetNameState extends State {
    protected SetNameState(State state) {
        super(state);
    }

    @Override
    public State next() {
        System.out.println(resource.getString("setName"));
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextLine()) {
            String name = scanner.nextLine();
            pudge.setName(name);
        }
        return new SetLevelState(this);
    }
}

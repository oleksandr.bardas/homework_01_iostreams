package com.itea.state;

import com.itea.PudgeRepository;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SaveState extends State {
    protected SaveState(State state) {
        super(state);
    }

    @Override
    public State next() {
        PudgeRepository.save(pudge);
        return new FinishState(this);
    }
}

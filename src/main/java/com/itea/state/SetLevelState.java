package com.itea.state;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.InputMismatchException;
import java.util.Scanner;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SetLevelState extends State {
    protected SetLevelState(State state) {
        super(state);
    }

    @Override
    public State next() {
        Scanner scanner = new Scanner(System.in);
        Integer level = null;
        System.out.println(resource.getString("setLevel"));
        do {
            try {
                if (scanner.hasNextInt()) {
                    level = scanner.nextInt();
                } else {
                    scanner.next();
                }
            } catch (InputMismatchException e) {
                System.out.println(resource.getString("setLevel_tryAgain"));
                scanner.next();
            }
        } while (level == null);
        pudge.setLevel(level);
        return new AddPhraseState(this);
    }
}

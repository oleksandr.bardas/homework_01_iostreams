package com.itea.state;


import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AddPhraseState extends State {
    protected AddPhraseState(State state) {
        super(state);
    }

    @Override
    public State next() {
        System.out.println(resource.getString("addPhrase"));
        Scanner scanner = new Scanner(System.in);
        String phrase = scanner.nextLine();
        if (!phrase.isEmpty()) {
            List<String> phrases;
            if (pudge.getPhrases() != null) {
                phrases = new ArrayList<>(Arrays.asList(pudge.getPhrases()));
            } else {
                phrases = new ArrayList<>();
            }
            phrases.add(phrase);
            pudge.setPhrases(phrases.toArray(new String[0]));
            return this;
        } else {
            return new SaveState(this);
        }
    }
}

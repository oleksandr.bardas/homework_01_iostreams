package com.itea.state;

import com.itea.Pudge;
import com.itea.PudgeRepository;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Optional;
import java.util.ResourceBundle;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class LookUpState extends State {
    public LookUpState(ResourceBundle resource) {
        super(new Pudge(), resource);
    }

    @Override
    public State next() {
        Optional<Pudge> optionalPudge = PudgeRepository.findPudge();
        if (optionalPudge.isPresent()) {
            String name = optionalPudge.get().getName();
            pudge.setName(name);
            System.out.println(resource.getString("defaultName") + name);
            return new SetLevelState(this);
        } else {
            return new SetNameState(this);
        }
    }
}

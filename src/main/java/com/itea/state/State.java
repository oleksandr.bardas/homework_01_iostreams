package com.itea.state;

import com.itea.Pudge;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ResourceBundle;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
public abstract class State {
    protected Pudge pudge;
    protected ResourceBundle resource;

    protected State(State state) {
        if (state.pudge != null) {
            this.pudge = new Pudge(state.pudge);
        }
        this.resource = state.resource;
    }

    public abstract State next();
}

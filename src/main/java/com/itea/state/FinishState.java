package com.itea.state;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FinishState extends State {
    protected FinishState(State state) {
        super(state);
    }

    @Override
    public State next() {
        return this;
    }
}

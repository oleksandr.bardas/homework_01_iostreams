package com.itea;

import com.itea.state.FinishState;
import com.itea.state.LookUpState;
import com.itea.state.State;

import java.util.Locale;
import java.util.ResourceBundle;

public class PudgeMachine implements Runnable {
    private State state;

    public PudgeMachine(Locale locale) {
        ResourceBundle resource = ResourceBundle.getBundle("MessageBundle", locale);
        this.state = new LookUpState(resource);
    }

    @Override
    public void run() {
        while (!(state instanceof FinishState)) {
            state = state.next();
        }
    }
}

package com.itea;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PudgeRepository {
    private static final String FILE_NAME = "pudge.txt";

    public static void save(Pudge pudge) {
        try (DataOutputStream out = new DataOutputStream(new FileOutputStream(FILE_NAME))) {
            out.writeUTF(pudge.getName());
            out.writeInt(pudge.getLevel());
            for (String phrase : pudge.getPhrases()) {
                out.writeUTF(phrase);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Fatal error", e);
        }
    }

    public static Optional<Pudge> findPudge() {
        try (DataInputStream in = new DataInputStream(new FileInputStream(FILE_NAME))) {
            Pudge pudge = new Pudge();
            String name = in.readUTF();
            pudge.setName(name);
            int level = in.readInt();
            pudge.setLevel(level);
            List<String> phrases = new ArrayList<>();
            try {
                while (in.available() > 0) {
                    phrases.add(in.readUTF());
                }
            } catch (IOException ignored) {
            }
            pudge.setPhrases(phrases.toArray(new String[0]));
            return Optional.of(pudge);
        } catch (FileNotFoundException e) {
            return Optional.empty();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Fatal error", e);
        }
    }
}

package com.itea;

import java.util.Locale;

public class App {
    public static void main(String[] args) {
        Locale locale = getLocale(args);
        PudgeMachine machine = new PudgeMachine(locale);
        machine.run();
    }

    private static Locale getLocale(String[] args) {
        String lang = null;
        String region = null;
        if (args.length > 0) {
            lang = args[0];
        }
        if (args.length > 1) {
            region = args[1];
        }

        return new Locale.Builder().setLanguage(lang).setRegion(region).build();
    }
}
